package com.teamsankya.webservices.rootclasses;

import java.util.Set;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.teamsankya.webservices.dao.StudenDAO;
import com.teamsankya.webservices.dto.ResponseGenerator;
import com.teamsankya.webservices.dto.StudentBean;

@Path("/student")
public class StudentRestApi {

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseGenerator addStudent(StudentBean bean) {
	
		StudenDAO dao = new StudenDAO();
		ResponseGenerator generator = new ResponseGenerator();
		if (dao.addStudent(bean)) {
			generator.setStatusCode(201);
			generator.setMessage("Student Creation Report");
			generator.setDescription("Student added Successfully..");
		} else {
			generator.setStatusCode(401);
			generator.setMessage("Student Failure Report");
			generator.setDescription("Unsuccessfull");
			
		}
		return generator;
		
	} 

	@GET
	@Path("/get/{regno}")

	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public StudentBean getStudent(@PathParam("regno") int regno) {
		StudenDAO dao = new StudenDAO();
		return dao.getStudent(regno);
	}

	@GET
	@Path("getAll")
	@Produces(MediaType.APPLICATION_JSON)
	//@Consumes(MediaType.APPLICATION_JSON)
	public Set<StudentBean> getAllStudent() {
		StudenDAO dao = new StudenDAO();
		return dao.getAllStudent();
	}

	@PUT
	@Path("update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public ResponseGenerator updateStudent(StudentBean bean) {

		ResponseGenerator generator = new ResponseGenerator();
		StudenDAO dao = new StudenDAO();
		if (dao.updateStudent(bean)) {
			generator.setStatusCode(202);
			generator.setMessage("Student Update");
			generator.setDescription("Student Details Updated Successfully");
		} else {
			generator.setStatusCode(402);
			generator.setMessage("Student Update");
			generator.setDescription("Student Details Failed to Update");
		}
		return generator;
	}

	@DELETE
	@Path("delete")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseGenerator deleteStudent(@QueryParam("regno") int regno) {
		StudenDAO dao = new StudenDAO();
		ResponseGenerator generator = new ResponseGenerator();
		if (dao.deleteStudent(regno)) {
			generator.setStatusCode(203);
			generator.setMessage("Student Delete");
			generator.setDescription("Student Deleted Successfully");
		} else {
			generator.setStatusCode(403);
			generator.setMessage("Student Delete");
			generator.setDescription("Student Details Failed to Delete");
		}
		return generator;
	}

	@GET
	@Path("/createcookie")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCookie() {
		ResponseGenerator generator = new ResponseGenerator();
		generator.setStatusCode(203);
		generator.setMessage("create cookie");
		generator.setDescription("successfully created");
		NewCookie cookie = new NewCookie("name", "rahul");

		ResponseBuilder builder = Response.ok().cookie(cookie).entity(generator);
		Response response = builder.build();
		return response;
	}

	@GET
	@Path("/readcookie")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseGenerator readCookie(@CookieParam("name") String name) {
		ResponseGenerator generator = new ResponseGenerator();
		generator.setStatusCode(203);
		generator.setMessage("read cookie");
		generator.setDescription("successfully read cookie" + name);
		return generator;
	}
}
