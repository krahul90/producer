package com.teamsankya.webservices.rootclasses;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;


@Path("/upload")
public class Fileupload {
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String upload(@FormDataParam("file") InputStream stream,
			@FormDataParam("file") FormDataContentDisposition fileDetails) throws IOException {
		if (stream != null || fileDetails != null) {
			byte[] b = new byte[1024];
			stream.read(b);
			File file = new File( fileDetails.getFileName());
			FileOutputStream fileOutputStream = new FileOutputStream("E:\\" +file);
			fileOutputStream.write(b);
			fileOutputStream.flush();
			fileOutputStream.close();
			return "file upload successfully";
		} else {
			return "file not uploaded";
		}
	}
}
