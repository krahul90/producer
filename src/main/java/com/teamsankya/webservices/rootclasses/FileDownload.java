package com.teamsankya.webservices.rootclasses;

import java.io.File;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/download")
public class FileDownload {
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response fileDownload(){
	File file = new File("E:\\download.text");
	Response resp=Response.ok().header(HttpHeaders.CONTENT_DISPOSITION,"attachment;filename='rahul.text'").build();
   return resp;
	}

}
