package com.teamsankya.webservices.application;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.media.multipart.MultiPartFeature;

import com.teamsankya.webservices.rootclasses.FileDownload;
import com.teamsankya.webservices.rootclasses.Fileupload;
import com.teamsankya.webservices.rootclasses.StudentRestApi;

@ApplicationPath("/api")
public class RestApplication extends Application {
	@Override
	public Set<Class<?>> getClasses() {
		HashSet<Class<?>> set = new HashSet<>();
		set.add(StudentRestApi.class);
		set.add(Fileupload.class);
		set.add(MultiPartFeature.class);
		set.add(FileDownload.class);
		return set;
	}
}
