package com.teamsankya.webservices.dao;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.teamsankya.webservices.dto.StudentBean;


public class StudenDAO {
	private static HashSet<StudentBean> hashSet = new HashSet<>();
	
	public boolean addStudent(StudentBean bean) {
		return hashSet.add(bean);
	}
	public StudentBean getStudent(int regno) {
		for (StudentBean studentBean : hashSet) {
			if(studentBean.getRegno()==regno) {
				return studentBean;
			}
		}
		return null;
	}

	public Set<StudentBean> getAllStudent(){
		return hashSet;
	}
	
	public boolean updateStudent(StudentBean bean) {
		Iterator<StudentBean> iterator = hashSet.iterator();
		while (iterator.hasNext()) {
			StudentBean studentBean = iterator.next();
			if(studentBean.getRegno()==bean.getRegno()) {
				studentBean.setName(bean.getName());
				studentBean.setMarks(bean.getMarks());
				studentBean.setAddress(bean.getAddress());
				studentBean.setGender(bean.getGender());
				return true;
			}
			
		}
		return false;
		
	}
	
	public boolean deleteStudent(int regno) {
		
		Iterator<StudentBean> iterator = hashSet.iterator();
		while(iterator.hasNext()) {
			StudentBean bean = iterator.next();
			if(bean.getRegno()==regno) {
				iterator.remove();
				return true;
			}
		}
		return false;
				
	}
	
}
